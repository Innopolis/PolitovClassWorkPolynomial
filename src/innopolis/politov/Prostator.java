package innopolis.politov;

/**
 * Created by General on 2/10/2017.
 */
public class Prostator extends Calculator  {
    public Prostator(CalculatorCunsumer consumer, int[] numbers) {
        super(1, consumer, numbers);
    }

    @Override
    public void notifyConsummer(int newSumVaule){
        consumer.message(0, 0, newSumVaule);
    }
}
