package innopolis.politov;

/**
 * Created by General on 2/10/2017.
 */
public class Kubator extends Calculator {
    public Kubator(CalculatorCunsumer consumer, int[] numbers) {
        super(3, consumer, numbers);
    }

    @Override
    public void notifyConsummer(int newSumVaule){
        consumer.message(newSumVaule, 0, 0);
    }
}
