package innopolis.politov;

/**
 * Created by General on 2/10/2017.
 */
public class CalculatorCunsumer {
    class CriticalSection{
        volatile  boolean busy = false;
        void enter(){
            while(busy){
                Thread.yield();
            }
            busy = true;
        }

        void leave(){
            busy = false;
        }
    }

    private CriticalSection busyKubator;
    private CriticalSection busyKvadrator;
    private CriticalSection busyProstator;

    public  volatile int kub = 0;
    public  volatile int kv = 0;
    public  volatile int pr = 0;

    public CalculatorCunsumer() {
        this.busyKubator = new CriticalSection();
        this.busyKvadrator = new CriticalSection();
        this.busyProstator = new CriticalSection();
    }

    public  void message(int  kubator, int kvadrator, int prostator){
        try {
            if (kubator != 0) {
                busyKubator.enter();
                kub = kubator;
            }
            if (kvadrator != 0) {
                busyKvadrator.enter();
                kv = kvadrator;
            }
            if (prostator != 0){
                busyProstator.enter();
                pr = prostator;
            }
//            System.out.println("Kubator  = " + kubator);
//            System.out.println("Kvadrator  = " + kvadrator);
//            System.out.println("Prostator  = " + prostator);

            System.out.println(kub + " + " + kv + " + " + pr
                    + " = " + (kub + kv + pr));
        }
        finally {
            if (kubator != 0)
                busyKubator.leave();
            if (kvadrator != 0)
                busyKvadrator.leave();
            if (prostator != 0)
                busyProstator.leave();
        }
    }


}
