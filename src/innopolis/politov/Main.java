package innopolis.politov;

import java.util.function.Consumer;

public class Main {

    public static void main(String[] args) {
        int[] mass = new int[]{1, 2, 3};

        CalculatorCunsumer consumer = new CalculatorCunsumer();

        Thread thKubator1 = new Thread(new Kubator(consumer , mass), "thKubator1");
        Thread thKvadrator1 = new Thread(new Kvadrator(consumer , mass), "thKvadrator1");
        Thread thProstator1 = new Thread(new Prostator(consumer , mass), "thProstator1");
        Thread thProstator2 = new Thread(new Prostator(consumer , mass),"thProstator2" );

        thKubator1.start();
        thKvadrator1.start();
        thProstator1.start();
        thProstator2.start();
    }
}
