package innopolis.politov;

/**
 * Created by General on 2/10/2017.
 */
public class Kvadrator extends Calculator {

    public Kvadrator(CalculatorCunsumer consumer, int[] numbers) {
        super(2, consumer, numbers);
    }

    @Override
    public void notifyConsummer(int newSumVaule){
        consumer.message(0, newSumVaule, 0);
    }
}
