package innopolis.politov;

/**
 * Created by General on 2/10/2017.
 */
public class Calculator implements Runnable {
    private int power = 0;
    protected CalculatorCunsumer consumer;
    private  int[] numbers;
    private int sum = 0;

    public Calculator(int power, CalculatorCunsumer consumer, int[] numbers) {
        this.power = power;
        this.consumer = consumer;
        this.numbers = numbers;
    }

    public void notifyConsummer(int newSumVaule){

    }

    @Override
    public void run() {
        sum = 0;
        for (int element:
                numbers) {
            sum += (long)Math.pow(element, power);
            notifyConsummer(sum);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
